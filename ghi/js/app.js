window.addEventListener('DOMContentLoaded', async () => {

    function createCard(name, description, pictureUrl, starts, ends, locationName) {
        return `
        <div class="col">
        <div class="col-sm-6">
            <div class="card">
                <img src="${pictureUrl}" class="card-img-top">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                <p class="card-text">${description}</p>
                </div>
                <div class="card-footer">
                    ${new Date(starts).toDateString()} - ${new Date(ends).toDateString()}
                </div>
            </div>
        </div>
        </div>
        `;
      }

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
          console.log('Error with Page');
        } else {
          const data = await response.json();

          for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const starts = details.conference.starts;
                const ends = details.conference.ends;
                const locationName = details.conference.location.name;
                const locationUrl = `http://localhost:8000${details.conference.location.href}`;
                const locationResponse = await fetch(locationUrl);
                if (locationResponse.ok) {
                    const locationDetails = await locationResponse.json();
                    var pictureUrl = locationDetails.picture_url;
                }
                const html = createCard(title, description, pictureUrl, starts, ends, locationName);
                const column = document.querySelector('.col');
                column.innerHTML += html;
            }
          }

        }
      } catch (e) {
        console.log(e);
        // Figure out what to do if an error is raised
      }

    });
