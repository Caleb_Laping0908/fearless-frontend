window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }

    const icon = document.getElementById('loading-conference-spinner');
    icon.classList.add('d-none');
    const list = document.getElementById('conference');
    list.classList.remove('d-none');


    const formTag = document.getElementById('create-attendee-form');
    formTag.addEventListener('submit', event => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceHref = JSON.parse(json)
        const attendeesUrl = `http://localhost:8001${conferenceHref.conference}attendees/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const attendeesResponse = fetch(attendeesUrl, fetchConfig);
        if (attendeesResponse.ok) {
            formTag.reset();
        }
        const successAlert = document.getElementById("success-message");
        formTag.classList.add("d-none")
        successAlert.classList.remove("d-none");

  });
  });
